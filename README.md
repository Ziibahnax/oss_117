# OSS_117

Le programme que je vous propose est un petit jeu très sympas, jouable deux joueurs **uniquement**.                                   
Pour télécharger et lancer le programme, rien de plus simple !
1.  Cliquez sur le **ficher** nommé *OSS_117.py* présent au dessus de *README.md*.
2.  Cliquez sur le bouton de *tééchargement* situé en **haut** et à **droite** de la fenètre avec le programme.
3.  Si vous êtes sous : 
*  **Linux avec un terminal**, installez *Visual Studio* grâce à ce lien : [https://code.visualstudio.com/docs/setup/linux](url)
*  **Windows**, installez *Visual Studio* grâce à ce lien : [https://visualstudio.microsoft.com/fr/downloads/](url)
4.  Ouvrez *Visual Studio*
5.  Allez sur *File* en **haut** à **gauche** de la fenètre de *Visual Studio* => *open file* => *OSS_117.py*.
6.  Vous êtes maintenant en mesure de mofifier ce fichier à votre guise!
7.  (FACULTATIF) Si *Visual Studio* vous propose d'installer un extension afin d'éxécuter le programme, faites-le.

Maintenant, comment jouer?  
Tous simplement en cliquant sur la petite *flèche verte* en **haut** à **droite** de l'écran.  
Le programme va alors gentiment vous souhaiter la bienvenue, ainsi que de presser sur *entrée* pour lire la suite.  
Il fera ensuit une tès brève présentation du jeu (pressez sur *entrée* ici aussi).  
Et vous demantera si vous connaissez ce jeu : 
*  Répondez **oui** (sans **ESPACES**) si vous connaissez ce jeu.
*  Répondez **non** (sans **ESPACES**) dans le cas contraire et vous présentera les règles de base.

Normalement, le programme devrait vous demandez d'insérer le *nom du joueur 1*, tapez alors votre pseudonyme ou prénom.  
Répétez l'opération pour le *nom du joueur 2*.  
Maintenant, le nombre de *manches gagnantes*. Cette valeur est par défaut à 2, il s'agit ici du nombre de *duels remportés* que le *gagnant* doit cumuler pour obtenir la *victoire finale*, mais vous pouvez la réduire à 1 ou l'augmenter autant que vous voulez ! 
Et le jeu commence (enfin) en proposant au *joueur 1* de choisir entre : *"cover" / "reload" / "shoot"* ou leurs abréviations correspondantes : *"c" / "r" / "s"*. Ces trois actions doivent être copiées à **l'identique** **sans espaces ni guillemets**!(Ce serait dommage de perdre un tour pour si peu)  
**cover** permet de **se protéger d'une attaque adverse** si ce dernier tire. *Attention! Cette action n'est valide que trois tours de suite, pas plus!*  
**reload** permet de **charger une balle** dans son révolver. *Attention! Vous ne pouvez cumuler que le nombre incroyable de une seule balle à la fois!*  
**shoot** permet de **tirer sur son adversaire** et de **gagner un duel**, à condition d'**avoir une balle chargée** et un **adversaire à découvert**.  
Puis en proposant la même chose au *joueur 2* et ainsi de suite jusqu'à ce que le gagnant franchisse le score limite.


Maintenant vous savez tout! **Good Luck and Have Fun**


*Ziibahnax*