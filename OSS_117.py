#------------------------------------------------------------------------IMPORTATION------------------------------------------------------------------------#

from random import choice, randint
from time import sleep  

#-------------------------------------------------------------------------FONCTIONS-------------------------------------------------------------------------#

    #Paramètres des joueurs 
player1_covers = 0
player1_ammo = 0
player1_command = ""
score_player1 = 0
player2_ammo = 0
player2_covers = 0
player2_command  = ""
score_player2 = 0
score = 0
round = 0

    #Déroulement à deux Joueurs
def deroulement_à_2joueurs():
    #Importation des variables
        #Du joueur 1
    global player1_command
    global player1_covers
    global player1_ammo
    global score_player1
        #Du joueur 2
    global player2_command
    global player2_covers
    global player2_ammo
    global score_player2
    global score
        #Mettre les commandes des deux joueur vides
    player1_command = ""
    player2_command = ""
        #Remplissage des commandes des deux joueurs
    while player1_command == "":
        player1_command = input( n1 + ' choisis : "cover" / "reload" / "shoot" ? ')
        print("\n"* 50)
    while player2_command == "":
        player2_command = input( n2 + ' choisis : "cover" / "reload" / "shoot" ? ')
        #Actions du joueur 1
            #Pour la commande de rechargement
        if player1_command == "reload" or player1_command == "r":
            player1_covers = 0
            if player1_ammo == 0:
                player1_ammo = player1_ammo + 1
                print(n1 + " recharge !")
            else:
                print(n1 + " est déja chargé a block !")
            #Pour la commande de défense
        elif player1_command == "cover" or player1_command == "c": 
            if player1_covers < 3:
                player1_covers += 1
                print(n1 + " se protège !")
            else:
                print(n1 + " ne peux plus se protéger !")
            #Pour la commande d'attaque
        elif player1_command == "shoot" or player1_command == "s" :
            player1_covers = 0
            if player1_ammo == 1:
                    #Avec défense adverse
                if player2_command == "cover" or player2_command == "c": 
                    if  player2_covers < 3:
                        print(n2 + " s'est protégé !")
                    #Quand la défense adverse rompt
                    else:
                        print("BANG !")
                        sleep(1)
                        print(n1 + " a tué " + n2 + " !")
                        score_player1 += 1
                        player2_ammo = 0
                        player2_covers = 0
                        player1_ammo = 0
                        player1_covers = 0
                    player1_ammo = 0
                    #Avec attaque adverse
                elif player2_command == "shoot" or player2_command == "s":
                    if player2_ammo == 1:
                        print( n1 + " et " + n2 + " se sont entretués !")
                        player1_ammo = 0
                        score_player1 += 1
                    else:
                        print("Click ! " + n2 + " est a sec !")
                #Sans action défensive/offensive adverse
                else:
                    print("BANG !")
                    sleep(1)
                    print(n1 + " a tué " + n2 + " !")
                    score_player1 += 1
                    player2_ammo = 0
                    player2_covers = 0
                    player1_ammo = 0
                    player1_covers = 0
                #Sans munitions
            else:                
                print("Click ! " + n1 + " est a sec !")
                #Erreur de syntaxe (Ceci passe votre tour ! Vous ferez mieux le prochaine fois !)
        else: 
            print("Ce n'est pas une action valide !")
        #Actions du joueur 2
        
            #Pour la commande de rechargement
        if player2_command == "reload" or player2_command == "r" :
            player2_covers = 0
            if player1_command == "shoot" or player1_command == "s":
                player2_ammo = 0
            elif player2_ammo == 0:
                player2_ammo = player2_ammo + 1
                print(n2 + " recharge !")
            else:
                print(n2 + " est déja chargé a block !")
            #Pour la commande de défense
        elif player2_command == "cover" or player2_command == "c": 
            if player2_covers < 3:
                player2_covers += 1
                print(n2 + " se protège !")
            else:
                print(n2 + " ne peux plus se protéger !")
            #Pour la commande d'attaque
        elif player2_command == "shoot" or player2_command == "s":
            player2_covers = 0
            if player2_ammo == 1:
                    #Avec défense adverse
                if player1_command == "cover" or player1_command == "c":
                    if  player1_covers < 3:
                        print(n1 + " s'est protégé !")
                    #Quand la défense adverse rompt
                    else:
                        print("BANG !")
                        sleep(1)
                        print(n2 + " a tué " + n1 + " !")
                        score_player2 += 1
                        player2_ammo = 0
                        player2_covers = 0
                        player1_ammo = 0
                        player1_covers = 0
                    player2_ammo = 0
                    #Avec attaque adverse
                elif player1_command == "shoot" or player1_command == "s":
                    player2_ammo = 0
                    score_player2 += 1
                    #Sans action défensive/offensive adverse
                else:
                    if player2_ammo == 1:
                        print("BANG !")
                        sleep(1)
                        print(n2 + " a tué " + n1 + " !")
                        score_player2 += 1
                        player2_ammo = 0
                        player2_covers = 0
                        player1_ammo = 0
                        player1_covers = 0
                    #Sans munitions
                    else:                
                        print("Click ! " + n2 + " est a sec !")
                #Erreur de syntaxe (Ceci passe votre tour ! Vous ferez mieux le prochaine fois !)
        else: 
            print("Ce n'est pas une action valide !")
        score = score_player1
    if score_player1 > score_player2:
        score = score_player1
        print( n1 + " gagne avec " + str(score) + " point(s) !")
    elif score_player1 == score_player2:
        print("égalité avec " + str(score) + " point(s) !")
    else:
        score = score_player2
        print( n2 + " gagne avec " + str(score) + " points(s) !")
    print(player2_ammo, player2_covers, player1_ammo, player1_covers)
    player1_command == ""
    player2_command == ""

    #Système de Rounds 
def round_system():
    while score < int(score_max):
        deroulement_à_2joueurs()
    if score == score_max:
        if score_player1 > score_player2:
            print(n1 + " a gagné la partie en " + str(round) + " tours avec " + str(score) + " points !")
        elif score_player1 == score_player2:
            print(n1 + " et " + n2 + " finissent ex aequo avec " + str(score) + " points !")
        else:
            print(n2 + " a gagné la partie en " + str(round) + " tours avec " + str(score) + " points !")

#----------------------------------------------------------------------PROGRAMME----------------------------------------------------------------------# 

#Introduction au jeu

input("\nBienvenue sur OS_117 ! (appuyez sur entrée)")
input("Ce petit jeu reprend les principes du jeu 007 que l'on connais tous !\n")
m = input("Connaissez vous ce jeu ? (oui / non) : \n -> ")
if m.lower() == "oui":
    sleep(1)
    print("\nTant mieux, commeçons alors !\n\n")
    sleep(1)
else:
    input("\nNon ? Vous n'avez peut être pas eu d'enfance après tout, voici un petit apreçu des règles !\n")
    input("Deux joueurs s'affrontent, trois options s'offrent à vous : tirer, recharger ou se protéger !")
    input("Mais attention ! Vous ne pourrez pas tirer si vous n'avez pas rechargé au préalable !")
    input("Vous ne pouvez également pas vous protéger plus de 3 fois de suite !")
    input("Mais aussi, vous êtes vulnérable lorsque vous rechargez, alors faites attention !\n")
    sleep(1)
    print("Prêt ? Commençons alors !\n\n")
    sleep(1)

#Joueur 1
n1 = input("Nom du joueur 1 : \n -> ")

#Joueur 2
n2 = input("Nom du joueur 2 : \n -> ")

#Sélection du Score Max 
score_max = input("Score gagnant : (défaut 2) \n -> ")
if score_max >= "1":
    print("Score maximum défini à " + str(score_max) + " duels remportés !")
else:
    score_max = "2"
    print("Score maximum défini à " + str(score_max) + " duels remportés !")
#Mise en Place du jeu 
print("Le jeu est sur le point de commencer !")
sleep(1)
print("3")
sleep(1)
print("2")
sleep(1)
print("1")
sleep(1)
print("GO !\n")
sleep(1)
print(n1 + " vs " + n2 + "\n")
round_system()